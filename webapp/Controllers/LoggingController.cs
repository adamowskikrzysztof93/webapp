﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace webapp.Controllers
{
    public class LoggingController : Controller
    {
        private readonly ILogger<LoggingController> _logger;

        public LoggingController(ILogger<LoggingController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        [Route("LogInfo")]
        public IActionResult LogInfo()
        {
            _logger.LogInformation("This is an informational log message.");
            return Ok();
        }

        [HttpPost]
        [Route("LogWarning")]
        public IActionResult LogWarning()
        {
            _logger.LogWarning("This is a warning log message.");
            return Ok();
        }

        [HttpPost]
        [Route("LogError")]
        public IActionResult LogError()
        {
            _logger.LogError("This is an error log message.");
            return Ok();
        }
    }
}
