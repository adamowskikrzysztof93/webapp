﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function logInfo() {
    $.ajax({
        type: "POST",
        url: "/LogInfo",
        success: function () {
            console.log("Information logged successfully.");
        },
        error: function () {
            console.error("Failed to log information.");
        }
    });
}

function logWarning() {
    $.ajax({
        type: "POST",
        url: "/LogWarning",
        success: function () {
            console.log("Warning logged successfully.");
        },
        error: function () {
            console.error("Failed to log warning.");
        }
    });
}

function logError() {
    $.ajax({
        type: "POST",
        url: "/LogError",
        success: function () {
            console.log("Error logged successfully.");
        },
        error: function () {
            console.error("Failed to log error.");
        }
    });
}